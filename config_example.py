DOMAIN = "http://demo.qiushichao.com/" # this is used for exp
PORT = 8080 # this is used for binding socket
enable_nginx = True # if enabled, get ip from headers["realip"]
EXPIRE_TIME = 600 # auto delete user data after EXPIRE_TIME