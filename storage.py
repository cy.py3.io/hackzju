import time
from config import EXPIRE_TIME
SERVER_STORAGE = {}

def __time():
    return int(time.time())
    
def __time_expired(oldtime):
    if oldtime == -1:
        return False
    return (__time()-oldtime)>EXPIRE_TIME

def storage_set(key,value):
    global SERVER_STORAGE
    SERVER_STORAGE[key]=value
    SERVER_STORAGE[key+"time"]=__time()

def storage_update(IP,dict):
    global SERVER_STORAGE
    SERVER_STORAGE.setdefault(IP,{}).update(dict)
    SERVER_STORAGE[IP+"time"]=__time()

def storage_get(key,default=None):
    global SERVER_STORAGE
    if key not in SERVER_STORAGE:
        return default
    if not __time_expired(SERVER_STORAGE.get(key+"time",-1)):
        return SERVER_STORAGE.get(key,default)
    else:
        del SERVER_STORAGE[key]
        return default

def storage_clear(IP):
    global SERVER_STORAGE
    if IP in SERVER_STORAGE:
        del SERVER_STORAGE[IP]