from EasyLogin import EasyLogin
import json

class ZUINFO():
    def __init__(self,cookiestring):
        self.cookiestring = cookiestring
        self.a = EasyLogin(cookiestring=cookiestring)
        self.xh = None
    
    def islogin(self):
        return False
    
    def fetch(self):
        pass

class ZUINFO_JWW(ZUINFO):
    DOMAIN = "http://jwbinfosys.zju.edu.cn/"
    def islogin(self):
        x=self.a.get(self.DOMAIN+"default2.aspx",o=True)
        if "xh=" not in x.headers.get("Location",""):
            return False
        self.xh=x.headers["Location"].split("xh=")[1].split("&")[0]
        return True
    
    def fetch(self):
        if self.xh == None:
            return Exception("call islogin() first")
        a = self.a
        a.get("http://jwbinfosys.zju.edu.cn/xsgrxx.aspx?xh="+self.xh)
        ourdata = {
            "home_address": a.b.find("span",{"id":"jtdz"}).text,
            "politics": a.b.find("select",{"id":"zzmm"}).find("option",{"selected":"selected"}).text,
            "high_school": a.b.find("span",{"id":"byzx"}).text,
            "dormitory": a.b.find("span",{"id":"ssh"}).text,
            "department": a.b.find("span",{"id":"xy"}).text,
            "class": a.b.find("span",{"id":"xzb"}).text,
            "father_name": a.b.find("input",{"name":"fqxm"}).get("value",""),
            "father_company": a.b.find("input",{"name":"fqdw"}).get("value",""),
            "father_phone": a.b.find("input",{"name":"fqdwdh"}).get("value",""),
            "mother_name": a.b.find("input",{"name":"mqxm"}).get("value",""),
            "mother_company": a.b.find("input",{"name":"mqdw"}).get("value",""),
            "mother_phone": a.b.find("input",{"name":"mqdwdh"}).get("value",""),
        }
        return ourdata

class ZUINFO_MYZJU(ZUINFO):
    DOMAIN = "http://my.zju.edu.cn/"
    def islogin(self):
        x=self.a.get(self.DOMAIN+"znew.do",o=True)
        if x.status_code==200:
            self.xh=True
            return True
        else:
            return False
    
    def fetch(self):
        if self.xh == None:
            return Exception("call islogin() first")
        data=json.loads(self.a.get(self.DOMAIN+"cardInfo/getCardAcc.do",result=False,o=True).json()["cardInfo"])
        ourdata={
            "sex": "男" if data["sex"]=="1" else "女",
            "phone": data["phone"].strip(),
            "xh": data["sno"].strip(),
            "card_amount": data["accamt"],
            "leave_date": data["expdate"],
            "sfz": data["identity"].strip(),
            "name": data["name"].strip(),
            "usecardnum": data["usecardnum"],
            "bankacc": data["bankacc"].strip(),
        }
        return ourdata